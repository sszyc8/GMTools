#include "UIEventManager.h"
#include <string>
#include "../ui/GMTool.h"

UIEventManager* UIEventManager::sUIManager = NULL;

UIEventManager::UIEventManager(void)
{
}


UIEventManager::~UIEventManager(void)
{
}
void UIEventManager::saveGmObject(GMTool* gm)
{
	this->_gm = gm;
}

GMTool* UIEventManager::getGmObject()
{
	return this->_gm;
}

void UIEventManager::updateUIMsg(char* msg)
{
	this->_gm->updateMsg(msg);
}

void UIEventManager::updateUICmd(QString pCmd)
{
	this->_gm->updateCmd(pCmd);
}