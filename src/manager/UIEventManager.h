#ifndef __UIEVENTMANAGER_H__
#define __UIEVENTMANAGER_H__

#include <QString>
class GMTool;
class UIEventManager
{
public:
	UIEventManager(void);
	~UIEventManager(void);

	static inline UIEventManager* getInstence()
	{
		if (!sUIManager)
		{
			sUIManager = new UIEventManager();
		}
		return sUIManager;
	}

	void saveGmObject(GMTool* gm);
	GMTool* getGmObject();
	void updateUIMsg(char* msg);		// 更新消息列表UI
	void updateUICmd(QString pCmd);		// 更新
private:
	static UIEventManager* sUIManager;

	GMTool* _gm;
};
#endif // !__UIEVENTMANAGER_H__

