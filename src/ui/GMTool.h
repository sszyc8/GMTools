#ifndef GMTOOLS_H
#define GMTOOLS_H

#include <QtWidgets/QMainWindow>
#include "ui_GMTool.h"

class GMTool : public QMainWindow
{
	Q_OBJECT

public:
	GMTool(QWidget *parent = 0);
	~GMTool();
	void updateMsg(char* msg);
	void updateCmd(QString qCmd);
private:
	Ui::GMToolsClass ui;
	void topWidget();
	void initCenter();
	void initBottomWidget();

public slots:
	void OnClickByHelp();
	void OnClickByAbout();
	void ClickByTopConnect();
	void OnClickByCneterCommit();
//	void ClickByBottomAdd();
//	void ClickByBottomDel();


};

#endif // GMTOOLS_H
